#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := $(call my-dir)

peripheralman_CommonCFlags := -Wall -Werror -Wno-unused-parameter
peripheralman_CommonCFlags += -Wno-sign-promo  # for libchrome
peripheralman_CommonCIncludes := \
  $(LOCAL_PATH)/../include \
  external/gtest/include \

peripheralman_CommonSharedLibraries := \
  libbinder \
  libbinderwrapper \
  libchrome \
  libutils \

# peripheralman executable
# ========================================================

include $(CLEAR_VARS)
LOCAL_MODULE := peripheralman
LOCAL_REQUIRED_MODULES := peripheralman.rc
LOCAL_CPP_EXTENSION := .cc
LOCAL_CFLAGS := $(peripheralman_CommonCFlags)
LOCAL_STATIC_LIBRARIES := \
  libperipheralman_internal \
  libperipheralman_binder \

LOCAL_SHARED_LIBRARIES := \
  $(peripheralman_CommonSharedLibraries) \
  libbrillo \
  libbrillo-binder \

LOCAL_SRC_FILES := main.cc

include $(BUILD_EXECUTABLE)

# peripheralman.rc script
# ========================================================

ifdef INITRC_TEMPLATE
include $(CLEAR_VARS)
LOCAL_MODULE := peripheralman.rc
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/$(TARGET_COPY_OUT_INITRCD)

include $(BUILD_SYSTEM)/base_rules.mk

$(LOCAL_BUILT_MODULE): $(INITRC_TEMPLATE)
	$(call generate-initrc-file,peripheralman,,)
endif

# libperipheralman_internal static lib
# ========================================================

include $(CLEAR_VARS)
LOCAL_MODULE := libperipheralman_internal
LOCAL_CPP_EXTENSION := .cc
LOCAL_CFLAGS := $(peripheralman_CommonCFlags)
LOCAL_C_INCLUDES := $(peripheralman_CommonCIncludes)
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/../include
LOCAL_STATIC_LIBRARIES := \
  libperipheralman_binder \

LOCAL_SHARED_LIBRARIES := \
  $(peripheralman_CommonSharedLibraries) \

LOCAL_SRC_FILES := \
  peripheral_manager.cc \


include $(BUILD_STATIC_LIBRARY)
